
$(document).ready(function () {

    $('#goUpBtn').hide()

    $("#menu").on("click", "a", function (event) {
        event.preventDefault();

        let id = $(this).attr('href')

        console.log(id);

        let top = $(id).offset().top;

        console.log(top);

        $('body,html').animate({scrollTop: top}, 1500);
    });

    $('#goUpBtn').click(function(){

        $('html,body').animate({ scrollTop: 0 }, 400);

    });

    $(document).on('scroll', function () {

        if (document.documentElement.scrollTop > 200) {

        $('#goUpBtn').show(500)

        } else {

            $('#goUpBtn').hide(500)
        }

    })
});
