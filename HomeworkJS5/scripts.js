// Задание
// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
//
//     Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

const createNewUser = (firstName, lastName, birthday) => {

    let newUser = {
        firstName,
        lastName,
        getLogin: () => {
            let changedFirstName = (firstName.charAt(0));
            return (changedFirstName+lastName).toLowerCase()
        },
        getAge: () => {
            let today = new Date(),
                changedBirthDate = `${birthday.slice(3,6)}${birthday.slice(0,3)}${birthday.slice(-4)}`,
                birthDate = new Date(changedBirthDate),
                age = today.getFullYear() - birthDate.getFullYear(),
                month = today.getMonth() - birthDate.getMonth();
            if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age
        },
        getPassword: () => {
            let firstLetter = firstName[0].toUpperCase(),
                lowLastName = lastName.toLowerCase(),
                userBirthdayYear = birthday.slice(-4)
            return `${firstLetter}${lowLastName}${userBirthdayYear}`
        }
    }
    return newUser
}

let user = createNewUser(prompt('Enter your First name','John'),prompt('Enter your Last name','Anderson'),prompt('When is your birthday?', '13.08.1991'))

console.log(`your login is: ${user.getLogin()}`);
console.log(`you are: ${user.getAge()} years old`);
console.log(`your new pass is: ${user.getPassword()}`);
console.log(user);
