// Our services nav 

$('.services-nav').on('click', (event) => {

    $('.services-nav-box-active').removeClass('services-nav-box-active');

    $('.services-content').each( (i, elem) => {
        if ($(event.target).data('name') === $(elem).first().data('name')) {
            $(event.target).addClass('services-nav-box-active');
            $(elem).show();
        } else {
            $(elem).hide();
        }
    });
});

// Cases nav

$(".cases-nav-box").click(function (event) {
    // создаем переменную с data-name елемента
    let dataName = $(this).data('name')

    if (dataName === 'All') {
        // если нажали кнопку All показать все елементы
        $(".gallery-box").show()
    }

    else $(".gallery-box").each( (i, elem) => {
        // сравнивает с нажатой кнопкой и фильтрует по data-name
        if ($(event.target).data('name') === $(elem).first().data('name')) {
            $(elem).show();
        }

        else {
            $(elem).hide();
        }
    });
})

// Load more btn

$(function(){

	$("#loadMore").on('click', function() {
    // при нажатии показывает + 4 картинки
		$(".box-hidden:hidden").slice(0, 4).slideDown();
    // если все картинки загружены - кнопка прячется
    if ($(".box-hidden:hidden").length == 0) {
      $("#loadMore").hide()
    } else $("#loadMore").show()

	});
});

// Our reviews

$(function(){

      //Creating text slides
      // Controller for the text slides
      $('.slides .tabs li').on('click', function(){
      // Variable used to navigate through the slide class
        let $tab = $(this).closest('.slides');
      // Finds the button that has the class tab-active and removes it
        $tab.find('.tabs li.tab-active').removeClass('tab-active');
        $tab.find('.tabs li.review-nav-img').removeClass('review-nav-img-active');
      //Adds the class tab-active to the button that was clicked
        $(this).addClass('tab-active');
        $(this).addClass('review-nav-img-active');
      // Variable to grab the rel attribute of the slide list
        let $newSlide = $(this).attr('rel');
      // Finds the slide that is currently active and hides it
        $tab.find('.slide-item.slide-visible').fadeOut(300, nextPanel);

      // Defining the callback that is activated when hiding active slide
        function nextPanel(){
      // Removes visible class from whichever slide is visible
          $(this).removeClass('slide-visible');
      // Matches rel value with the corresponding slide
          $('#' +$newSlide).fadeIn(300, function(){
      // Displays that slide
            $(this).addClass('slide-visible');
          });
       };
		});
});

//  next and prev buttons

$('.review-nav-arrow').on('click', function(){

      let $tab = $(this).closest('.slides');
      console.log($tab)
      // Finds the button that has the class tab-active and removes it
        $tab.find('.tabs li.tab-active').removeClass('tab-active');
        $tab.find('.tabs li.review-nav-img').removeClass('review-nav-img-active');
      //Adds the class tab-active to the button that was clicked
        // $('.tabs li.tab-active').addClass('tab-active');
        // $('.tabs li.review-nav-img').addClass('review-nav-img-active');
      // Variable to grab the rel attribute of the slide list
        // let $newSlide = $(this).attr('rel');
      // Finds the slide that is currently active and hides it
        // $tab.find('.slide-item.slide-visible').fadeOut(300);

      // Defining the callback that is activated when hiding active slide
      //   function nextPanel(){
      // // Removes visible class from whichever slide is visible
      //     $(this).removeClass('slide-visible');
      // // Matches rel value with the corresponding slide
      //     $('#' +$newSlide).fadeIn(300, function(){
      // // Displays that slide
      //       $(this).addClass('slide-visible');
      //     });
      //   }
      })