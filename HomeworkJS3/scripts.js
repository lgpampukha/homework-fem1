//Реализовать функцию, которая будет производить математические операции с введеными пользователем числами.
//
// Технические требования:
//
// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.
//
//
// Не обязательное задание продвинутой сложности:
//
// validation ()
// debugger
// После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).


// console.log(calculate());
calculate(prompt('Enter first number', '1'), prompt('Enter second number', '2'),prompt('Enter operation symbol', '+'));

function calculate (firstNum,secondNum,operationSymb) {
    const validatedFirstNumber = validation(firstNum);
    const validatedSecondNum = validation(secondNum);
    switch(operationSymb) {
        case "+":
            let plus = +validatedFirstNumber + +validatedSecondNum;
            console.log(plus);
            break;
        case "-":
            let minus = +validatedFirstNumber - +validatedSecondNum;
            console.log(minus);
            break;
        case "/":
            let divide = +validatedFirstNumber / +validatedSecondNum;
            console.log(divide);
            break;
        case "*":
            let multiply = +validatedFirstNumber * +validatedSecondNum;
            console.log(multiply);
            break;
    }
}
function validation (numberToValidate) {
    let validNum = numberToValidate;
    while (isNaN(validNum)
    || validNum === ''
    || validNum === null) {
        validNum = prompt('Enter number again', numberToValidate)
    }
    return validNum;
}