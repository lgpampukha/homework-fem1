function filterBy (arr, typeofdata) {
    if (typeofdata === 'string') {
        let arrayString = arr.filter(function (item) {
            return typeof item === 'string'
        })
        return arrayString
    }
    else if (typeofdata === 'number') {
        let arrayNumber = arr.filter(function (item) {
            return typeof item === 'number'
        })
        return arrayNumber
    }
    else if (typeofdata === 'boolean') {
        let arrayBollean = arr.filter(function (item) {
            return typeof item === 'boolean'
        })
        return arrayBollean
    }
}

let array = ['q', 'w', 'e', 1, 2, 3, true, false]
console.log(filterBy(array, 'string'));
console.log(filterBy(array, 'number'));
console.log(filterBy(array, 'boolean'));