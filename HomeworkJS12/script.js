window.onload = function () {
    let image = document.getElementById("aaa");
    let imgArray = ['./img/2.jpg', './img/3.JPG', './img/4.png', './img/1.jpg'];
    let index = 0;
    let isStoped = false

    function slide() {

        image.src = imgArray[index];
        index++;

        if (index >= imgArray.length) {
            index = 0;
        }
    }

    let timerId = setInterval(slide, 5000);



    let stopBtn = document.getElementById('stopBtn')
    stopBtn.addEventListener('click', function () {
        clearTimeout(timerId);
        isStoped = true
        console.log(isStoped);
    })

    let goBtn = document.getElementById('goBtn')
    goBtn.addEventListener('click', function () {
        if (isStoped) {
            setInterval(slide, 5000)
            isStoped = false
        }
        console.log(isStoped);
    })
}

