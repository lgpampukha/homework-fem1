// let chooseChamp = function (event) {
//     let i
//     let tabsText
//     let tabsTitle
//
//     tabsText = document.getElementsByClassName('tabs-text')
//     tabsTitle = document.getElementsByClassName('tabs-title')
//
//
//     for (i = 0; i < tabsText.length; i++) {
//
//         if (event.currentTarget.dataset.id !== tabsText[i].dataset.id) {
//             tabsText[i].style.display = 'none'
//         } else {
//             tabsText[i].style.display = 'block'
//         }
//     }
//
//
//     for (i = 0; i < tabsTitle.length; i++) {
//         tabsTitle[i].className = tabsTitle[i].className.replace(" active", "");
//     }
//
//     event.currentTarget.className += " active";
// }

// let champions = document.querySelectorAll('.tabs-title')
// for (let i = 0; i < champions.length; i++) {
//     champions[i].addEventListener('click', chooseChamp);
// }

let tabsList = document.querySelector('.tabs')
let tabsText = document.getElementsByClassName('tabs-text')
let tabsTitle = document.getElementsByClassName('tabs-title')

tabsList.addEventListener('click', function (event) {

    for (let i = 0; i < tabsText.length; i++) {
        if (event.target.dataset.id !== tabsText[i].dataset.id) {
            tabsText[i].style.display = 'none'
        } else {
            tabsText[i].style.display = 'block'
        }
    }

    for (let i = 0; i < tabsTitle.length; i++) {
        tabsTitle[i].className = tabsTitle[i].className.replace(" active", "");
    }

    event.target.className += " active";
})