// let blackTheme = document.querySelector('.black-theme')
$('.black-theme').hide()


let mainTheme = document.querySelector('.main-theme')
let flag = false
let attr = document.getElementById('style').attributes

window.onload = function () {

    if (localStorage.getItem('mainTheme')) {

        attr[2].value = localStorage.getItem('mainTheme')

    } else if (localStorage.getItem('newTheme')) {

        attr[2].value = localStorage.getItem('newTheme')

    } else {

        attr[2].value = 'css/style.css'
    }
}

mainTheme.addEventListener('click', function () {

    if (!flag) {

        localStorage.removeItem('mainTheme')
        localStorage.setItem('newTheme', 'css/style2.css')
        let newTheme = localStorage.getItem('newTheme')

        attr[2].value = newTheme

        flag = true

    } else {

        localStorage.removeItem('newTheme')
        localStorage.setItem('mainTheme', 'css/style.css')
        let mainTheme = localStorage.getItem('mainTheme')

        attr[2].value = mainTheme

        flag = false
    }


})