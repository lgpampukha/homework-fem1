// Задание
// Реализовать функцию для создания объекта "пользователь".
//
//     Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//
//     Не обязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

// debugger

const createNewUser = (firstName, lastName) => {

    let newUser = {
        first: firstName,
        second: lastName,
        getLogin: () => {
            let changedFirstName = (firstName.charAt(0));
            return (changedFirstName+lastName).toLowerCase()
        }
     }
     return newUser
}

let user = createNewUser(prompt('Enter your First name','John'),prompt('Enter your Last name','Anderson'))
console.log(user.getLogin())
