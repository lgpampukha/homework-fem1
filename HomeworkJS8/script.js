window.onload = function () {
    let whatPrice = document.createElement('input')
    whatPrice.placeholder = 'Price'
    whatPrice.type = 'number'
    whatPrice.style.display = 'block'
    document.body.appendChild(whatPrice)

    whatPrice.addEventListener('focus', () => {
        whatPrice.style.border = '4px solid green'
        whatPrice.style.color = 'black'


        clearAll()


    });

    whatPrice.addEventListener('blur', () => {

        let inputValue = whatPrice.value

        if (inputValue < 0) {

            whatPrice.style.border = '3px solid red'
            whatPrice.style.color = 'red'

            let bottomSpan = document.createElement('span')
            bottomSpan.className = 'bottomSpan'
            bottomSpan.innerHTML = `Please enter correct price`
            document.body.appendChild(bottomSpan)

        } else {

            let topSpan = document.createElement('span')
            topSpan.className = 'topSpan'
            topSpan.innerHTML = `Текущая цена: ${inputValue}`
            document.body.insertBefore(topSpan, whatPrice)

            let cancelBtn = document.createElement('button')
            cancelBtn.className = 'cancelBtn'
            cancelBtn.innerHTML = 'X'
            document.body.insertBefore(cancelBtn, whatPrice)


            whatPrice.style.border = '3px solid green'
            whatPrice.style.color = 'green'

            clearX()

        }

    })

    let clearX = function () {
        let clearTopSpan = document.querySelector('.topSpan')
        let clearBtn = document.querySelector('.cancelBtn')

        clearBtn.addEventListener('click', function () {
            clearTopSpan.remove()
            clearBtn.remove()
            
            if (whatPrice.value !== whatPrice.defaultValue) {
                whatPrice.value = ''
            }
        })



    }

    let clearAll = function () {
        let clearTopSpan = document.querySelector('.topSpan')
        let clearBtn = document.querySelector('.cancelBtn')
        let clearBottomSpan = document.querySelector('.bottomSpan')

        if (clearTopSpan && clearBtn) {

            clearTopSpan.remove()
            clearBtn.remove()

        } else if (clearBottomSpan) {

            clearBottomSpan.remove()
        }
    }


}

