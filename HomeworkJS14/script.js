$('.tabs-title').click(function () {

    let tabsData = $(this).attr('data-id')

    $('.tabs-title').removeClass('active')
    $(this).addClass('active')

    $('.tabs-text').removeClass('active')
    $('.tabs-text[data-id = '+tabsData+']').addClass('active')
})