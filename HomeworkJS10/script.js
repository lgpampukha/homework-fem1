let typeImgNormal = document.querySelectorAll('.fa-eye')

let firstNormalImg = document.querySelector('.firstNormalImg')
let firstSlashImg = document.querySelector('.firstSlashImg')
let secondNormalImg = document.querySelector('.secondNormalImg')
let secondSlashImg = document.querySelector('.secondSlashImg')

let firstInput = document.getElementById('firstInput')
let secondInput = document.getElementById('secondInput')

let mergeBtn = document.querySelector('.btn')
let form = document.querySelector('.password-form')
let error = document.createElement('span')


typeImgNormal.forEach(function (elem) {
    elem.style.display = 'none'
})

firstSlashImg.addEventListener('click', function () {
    firstSlashImg.style.display = 'none'
    firstNormalImg.style.display = ''
    if (firstInput.type === 'password') {
            firstInput.type = 'text'
    }
})

firstNormalImg.addEventListener('click', function () {
    firstNormalImg.style.display = 'none'
    firstSlashImg.style.display = ''
    if (firstInput.type === 'text') {
        firstInput.type = 'password'
    }
})

secondSlashImg.addEventListener('click', function () {
    secondSlashImg.style.display = 'none'
    secondNormalImg.style.display = ''
    if (secondInput.type === 'password') {
        secondInput.type = 'text'
    }
})

secondNormalImg.addEventListener('click', function () {
    secondNormalImg.style.display = 'none'
    secondSlashImg.style.display = ''
    if (secondInput.type === 'text') {
        secondInput.type = 'password'
    }
})

mergeBtn.addEventListener('click', function () {
    if (firstInput.value === secondInput.value) {
        if (error) {
            alert('You are welcome')
            error.remove()
        }
    } else {
        error.innerText = `Нужно ввести одинаковые значения`
        error.style.color = 'red'
        error.style.marginBottom = '10px'
        form.insertBefore(error, mergeBtn)
    }
})
